package io.github.diefonk.randomloot;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public class CommandLoot implements CommandExecutor {

	public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
		if (src instanceof Player) {
			Player player = (Player) src;
			player.getInventory().offer(LootCreation.getLoot());
		} else {
			src.sendMessage(Text.of("Command needs to be executed by a player"));
		}
		return CommandResult.success();
	}

}
