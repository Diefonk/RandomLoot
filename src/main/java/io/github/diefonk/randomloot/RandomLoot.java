package io.github.diefonk.randomloot;

import java.io.File;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.config.DefaultConfig;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;

import com.google.inject.Inject;

import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.loader.ConfigurationLoader;

@Plugin(id = "randomloot", name = "Random Loot", version = "0.5")
public class RandomLoot {

	@Inject
	@DefaultConfig(sharedRoot = true)
	private File configFile;

	@Inject
	@DefaultConfig(sharedRoot = true)
	private ConfigurationLoader<CommentedConfigurationNode> configManager;

	@Listener
	public void onServerStart(GameStartedServerEvent event) {
		ConfigManager.getInstance().setup(configFile, configManager);
		LootCreation.loadConfig();

		CommandSpec reload = CommandSpec.builder()
				.executor(new CommandReload())
				.build();
		CommandSpec loot = CommandSpec.builder()
				.description(Text.of("Gives you loot"))
				.child(reload, "reload", "r")
				.executor(new CommandLoot())
				.build();
		Sponge.getCommandManager().register(this, loot, "loot");
	}
}
