package io.github.diefonk.randomloot;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.text.Text;

public class CommandReload implements CommandExecutor {

	public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
		ConfigManager.getInstance().loadConfig();
		LootCreation.loadConfig();
		src.sendMessage(Text.of("Config reloaded"));
		return CommandResult.success();
	}

}
