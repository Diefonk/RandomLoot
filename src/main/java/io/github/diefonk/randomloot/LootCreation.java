package io.github.diefonk.randomloot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;

import com.google.common.reflect.TypeToken;
import com.google.inject.name.Names;
import com.typesafe.config.Config;

import ninja.leaping.configurate.objectmapping.ObjectMappingException;

public class LootCreation {
	private static Random rand = new Random();
	private static int nProb;
	private static ArrayList<Config> nItems;
	private static ArrayList<Config> uItems;
	private static HashMap<String, ArrayList<String>> names;
	private static HashMap<String, ArrayList<String>> prefixes;
	private static HashMap<String, ArrayList<String>> suffixes;
	private static HashMap<String, ArrayList<Config>> enchants;

	public static void loadConfig() {
		List<Config> tnItems;
		List<Config> tuItems;
		List<Config> tnames;
		List<Config> tprefixes;
		List<Config> tsuffixes;
		List<Config> tenchants;
		try {
			nProb = ConfigManager.getInstance().getConfig().getNode("nProb").getInt();
			tnItems = ConfigManager.getInstance().getConfig()
					.getNode("nItems")
					.getList(TypeToken.of(Config.class));
			tuItems = ConfigManager.getInstance().getConfig()
					.getNode("uItems")
					.getList(TypeToken.of(Config.class));
			tnames = (ArrayList<Config>) ConfigManager.getInstance().getConfig()
					.getNode("names")
					.getList(TypeToken.of(Config.class));
			tprefixes = (ArrayList<Config>) ConfigManager.getInstance().getConfig()
					.getNode("prefixes")
					.getList(TypeToken.of(Config.class));
			tsuffixes = (ArrayList<Config>) ConfigManager.getInstance().getConfig()
					.getNode("suffixes")
					.getList(TypeToken.of(Config.class));
			tenchants = (ArrayList<Config>) ConfigManager.getInstance().getConfig()
					.getNode("enchants")
					.getList(TypeToken.of(Config.class));
		} catch (ObjectMappingException e) {
			e.printStackTrace();
			System.out.println("Config could not be loaded!");
			return;
		}
		
		nItems = new ArrayList<Config>();
		uItems = new ArrayList<Config>();
		names = new HashMap<String, ArrayList<String>>();
		prefixes = new HashMap<String, ArrayList<String>>();
		suffixes = new HashMap<String, ArrayList<String>>();
		enchants = new HashMap<String, ArrayList<Config>>();
		
		for (int i = 0; i < tnItems.size(); i++) {
			for (int k = 0; k < tnItems.get(i).getInt("prob"); k++) {
				nItems.add(tnItems.get(i));
			}
		}
		for (int i = 0; i < tuItems.size(); i++) {
			for (int k = 0; k < tuItems.get(i).getInt("prob"); k++) {
				uItems.add(tuItems.get(i));
			}
		}
		names = getMap(tnames);
		prefixes = getMap(tprefixes);
		suffixes = getMap(tsuffixes);
		for (int i = 0; i < tenchants.size(); i++) {
			for (int k = 0; k < tenchants.get(i).getList("id").size(); k++) {
				if (!enchants.containsKey(tenchants.get(i).getList("id").get(k).toString())) {
					enchants.put(tenchants.get(i).getList("id").get(k).toString(), new ArrayList<Config>());
				} else {
					for (int j = 0; j < tenchants.get(i).getInt("prob"); j++) {
						enchants.get(tenchants.get(i).getList("id").get(k).toString()).add(tenchants.get(i));
					}
				}
			}
		}
	}

	public static ItemStack getLoot() {
		if (rand.nextInt(100) + 1 <= nProb) {
			return getNamed();
		}
		return getUnnamed();
	}
	
	private static ItemStack getNamed() {
		// TODO Auto-generated method stub
		return null;
	}
	
	private static ItemStack getUnnamed() {
		Config item = uItems.get(rand.nextInt(uItems.size()));
		ItemStack.Builder lootBuild = Sponge.getRegistry().createBuilder(ItemStack.Builder.class);
		lootBuild.itemType(Sponge.getRegistry().getType(ItemType.class, item.getString("id")));
		return null;
	}

	private static HashMap<String, ArrayList<String>> getMap(List<Config> l) {
		HashMap<String, ArrayList<String>> tmp = new HashMap<String, ArrayList<String>>();
		for (int i = 0; i < l.size(); i++) {
			for (int k = 0; k < l.get(i).getList("id").size(); k++) {
				if (!tmp.containsKey(l.get(i).getList("id").get(k).toString())) {
					tmp.put(l.get(i).getList("id").get(k).toString(), new ArrayList<String>());
				} else {
					for (int j = 0; j < l.get(i).getInt("prob"); j++) {
						tmp.get(l.get(i).getList("id").get(k).toString()).add(l.get(i).getString("s"));
					}
				}
			}
		}
		return tmp;
	}
}
