package io.github.diefonk.randomloot;

import java.io.File;
import java.io.IOException;

import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.loader.ConfigurationLoader;

public class ConfigManager {
	private static ConfigManager instance = new ConfigManager();

	public static ConfigManager getInstance() {
		return instance;
	}

	private ConfigurationLoader<CommentedConfigurationNode> configLoader;
	private CommentedConfigurationNode config;

	public void setup(File configFile, ConfigurationLoader<CommentedConfigurationNode> configLoader) {
		this.configLoader = configLoader;

		if (!configFile.exists()) {
			try {
				configFile.createNewFile();
				loadConfig();
				config.getNode("weapons").setValue(null);
				config.getNode("tools").setValue(null);
				config.getNode("armor").setValue(null);
				config.getNode("blocks").setValue(null);
				config.getNode("misc").setValue(null);
				saveConfig();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			loadConfig();
		}
	}

	public CommentedConfigurationNode getConfig() {
		return config;
	}

	private void saveConfig() {
		try {
			configLoader.save(config);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void loadConfig() {
		try {
			config = configLoader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
